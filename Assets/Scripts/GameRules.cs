﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Tilemaps;
using Assets.Scripts.Extensions;

namespace Assets.Scripts
{
    public class GameRules : MonoBehaviour
    {
        public bool AreDwarvesRed = true; //dwarves always start, so dwarves would be red first
        public TileBase RedTroll;
        public TileBase GreenTroll;
        public TileBase RedDwarve;
        public TileBase GreenDwarve;
        public TileBase SelectionIconTile;
        public TileBase PossibleMoveIconTile;
        public TileBase PossibleHurlIconTile;

        private TileBase DwarveTile;
        private TileBase TrollTile;

        public TileBase DebugTile;
        public PieceType CurrentTurn { get; private set; }
        public bool GameEndedManually { get; private set; }

        public IPiece CurrentlySelected { get; set; }

        public GameObject WelcomeScreen;

        private IList<Move> _CurrentValidMoves;

        private GameBoard _BoardInitialSetup;

        private PieceType[,] _PiecePositions; //logical positions

        private Tilemap _PiecesTilemap; //positions actually drawn

        private Tilemap _SelectionTilemap; //current selected piece, and possible moves

        private Tilemap _DebugTilemap;

        private int _BoardWidth;
        private Move _LastMove;

        public bool MustChooseDwarveToKill { get; private set; }

        public void Start()
        {
            GameObject boardObj = GameObject.Find("BoardGrid"); //TODO - find better way to include this dependency
            _BoardInitialSetup = boardObj.GetComponentInChildren<GameBoard>();
            _PiecesTilemap = GetComponent<Tilemap>();
            _BoardWidth = _BoardInitialSetup.BoardWidth;
            _PiecePositions = new PieceType[_BoardWidth, _BoardWidth];
            _CurrentValidMoves = new List<Move>();

            var selectionTilemapObj = boardObj.FindChild("Selection", true);
            _SelectionTilemap = selectionTilemapObj.GetComponent<Tilemap>(); //current selected piece, and possible moves
            _SelectionTilemap.ClearAllTiles();

            var debugTilemapObj = boardObj.FindChild("Debug", true);
            _DebugTilemap = debugTilemapObj.GetComponent<Tilemap>();
            
            ResetBoard(true);
        }


        public void Update()
        {
            CheckPieceSelection();
        }

        public void ResetBoard(bool redToStart) {
            GameEndedManually = false;
            AreDwarvesRed = redToStart;
            MustChooseDwarveToKill = false;
            CurrentlySelected = null;
            _CurrentValidMoves = new List<Move>();
            ChoosePieceSet();
            CurrentTurn = PieceType.Dwarves;
            ResetStartingPositions();
            DrawPieces();
        }

        private void ChoosePieceSet() {
            if (AreDwarvesRed)
            {
                DwarveTile = RedDwarve;
                TrollTile = GreenTroll;
            }
            else {
                DwarveTile = GreenDwarve;
                TrollTile = RedTroll;
            }
                
        }

        public void ResetStartingPositions() {
            _PiecePositions = new PieceType[_BoardInitialSetup.BoardWidth, _BoardInitialSetup.BoardWidth];
            for (int y = 0; y < _BoardInitialSetup.BoardWidth; y++)
            {
                for (int x = 0; x < _BoardInitialSetup.BoardWidth; x++)
                {
                    var boardPos = _BoardInitialSetup._BoardLayout[x, y];
                    if (boardPos.IsStartingPosition) {
                        _PiecePositions[x, y] = boardPos.StartingPositionType;
                    }
                }
            }
        }

        public void DrawPieces() {
            _PiecesTilemap.ClearAllTiles();

            for (int y = 0; y < _BoardInitialSetup.BoardWidth; y++)
            {
                for (int x = 0; x < _BoardInitialSetup.BoardWidth; x++)
                {
                    SetTile(_PiecePositions[x, y], x, y);                    
                }
            }
        }

        private void SetTile(PieceType piece, int x, int y) {

            if (piece == PieceType.Dwarves)
            {
                _PiecesTilemap.SetTile(new Vector3Int(x, y, 0), DwarveTile);
            }
            else if (piece == PieceType.Trolls)
            {
                _PiecesTilemap.SetTile(new Vector3Int(x, y, 0), TrollTile);
            }
        }
        private bool IsValidPieceCoordinate(int x, int y) {
            return 
                x >= 0 && 
                x < _PiecePositions.GetLength(0) && 
                y >= 0 && 
                y < _PiecePositions.GetLength(1);
        }



        public GameResult CalculateGameResult() {
            ScoreDetails redScore = new ScoreDetails();
            ScoreDetails greenScore = new ScoreDetails();
            int dwarvesScore = _PiecePositions.Cast<PieceType>().Count(x => x == PieceType.Dwarves) ;
            int trollsScore =  _PiecePositions.Cast<PieceType>().Count(x => x == PieceType.Trolls) * 4;

            redScore.Side = AreDwarvesRed ? PieceType.Dwarves : PieceType.Trolls;
            redScore.Score = AreDwarvesRed ? dwarvesScore : trollsScore;
            greenScore.Side = !AreDwarvesRed ? PieceType.Dwarves : PieceType.Trolls;
            greenScore.Score = !AreDwarvesRed ? dwarvesScore : trollsScore;
            GameResult result = new GameResult(redScore, greenScore);
            return result;
        }

        private bool CanSelect(int x, int y) {
            if (_PiecePositions == null || !IsValidPieceCoordinate(x, y))
                return false;

            return (_PiecePositions[x, y] == CurrentTurn);
        }

        public void CheckPieceSelection() {

            if (WelcomeScreen != null && WelcomeScreen.activeInHierarchy)
                return;

            if (Input.GetMouseButtonDown(0))
            {
                Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Vector3Int coordinate = _PiecesTilemap.WorldToCell(mouseWorldPos);
                Debug.Log(coordinate);

                _SelectionTilemap.ClearAllTiles();

                if (IsGameEnded)
                    return;

                if (MustChooseDwarveToKill )
                {
                    if (_PiecePositions[coordinate.x, coordinate.y] == PieceType.Dwarves && 
                        PieceCalculations.IsAdjacentTo(_LastMove.ToPosition, coordinate, PieceType.Dwarves, _BoardInitialSetup._BoardLayout, _PiecePositions))
                    {
                        _PiecePositions[coordinate.x, coordinate.y] = PieceType.None;
                        MustChooseDwarveToKill = false;
                        if (!IsGameEnded)
                            ToggleTurn();

                        DrawPieces();
                    }
                    return;
                }

                if (CanMove(coordinate))
                {
                    _LastMove = _CurrentValidMoves.First(pos => pos.ToPosition.x == coordinate.x && pos.ToPosition.y == coordinate.y);
                    Move(CurrentlySelected.Position, _LastMove); 
                    return;
                }

                if (CanSelect(coordinate.x, coordinate.y))
                {
                    var debugInfo = new DebugTilemapInfo()
                    {
                        DebugTilemap = _DebugTilemap,
                        DebugIconTile = DebugTile
                    };
                    debugInfo.ClearTiles();

                    CurrentlySelected = PieceFactory.GetPiece(_PiecePositions[coordinate.x, coordinate.y], coordinate, debugInfo);
                    _CurrentValidMoves = CurrentlySelected.GetValidMoves(_PiecePositions, _BoardInitialSetup._BoardLayout);
                    _SelectionTilemap.SetTile(coordinate, SelectionIconTile);
                    foreach (var move in _CurrentValidMoves)
                    {
                        if(move.Type == MoveType.Normal)
                            _SelectionTilemap.SetTile(move.ToPosition, PossibleMoveIconTile);
                        else
                            _SelectionTilemap.SetTile(move.ToPosition, PossibleHurlIconTile);
                    }
                }
                else {
                    CurrentlySelected = null;
                }
            }
        }


        private void Move(Vector3Int oldPosition, Move move )
        {
            var piece = _PiecePositions[oldPosition.x,oldPosition.y];
            _PiecePositions[move.ToPosition.x, move.ToPosition.y] = piece;
            _PiecePositions[oldPosition.x, oldPosition.y] = PieceType.None;
            _LastMove = move;
            // Troll capture rules
            if (CurrentlySelected.Type == PieceType.Trolls) {
                
                if (move.Type == MoveType.Hurl)
                    KillAdjacentDwarves(move.ToPosition);
                else if (move.Type == MoveType.Normal && //determine if there's a dwarf nearby to kill
                         PieceCalculations.IsAdjacentTo(move.ToPosition, PieceType.Dwarves, _BoardInitialSetup._BoardLayout, _PiecePositions))
                {
                    
                    MustChooseDwarveToKill = true;
                }
            }
            
            if (!IsGameEnded && !MustChooseDwarveToKill)
                ToggleTurn();

            DrawPieces();
        }

        public void EndGame() {
            GameEndedManually = true;
        }

        public bool IsGameEnded
        {
            get
            {
                if (GameEndedManually)
                    return true;

                GameResult result = new GameResult();
                List<PieceType> list = _PiecePositions.Cast<PieceType>().ToList();
                int trollCount = list.Count(x => x == PieceType.Trolls);
                int dwarveCount = list.Count(x => x == PieceType.Dwarves);

                return trollCount == 0 || dwarveCount == 0;
            }
        }

        private void KillAdjacentDwarves(Vector3Int toPosition)
        {
            var directions = PieceCalculations.GetPossibleDirections();
            foreach (var dir in directions) {
                var square = toPosition + dir;

                if (_PiecePositions[square.x, square.y] == PieceType.Dwarves)
                    _PiecePositions[square.x, square.y] = PieceType.None;
            }
        }

        bool CanMove(Vector3Int newLocation) {
            if (MustChooseDwarveToKill)
                return false;

            if (CurrentlySelected == null)
                return false;

            if (CurrentlySelected.Position == newLocation)
                return false;

            var tile = _BoardInitialSetup._BoardLayout[newLocation.x, newLocation.y].TileType;

            if (tile == BoardTileType.NotAccessible || tile == BoardTileType.ThudStone)
                return false;

            if (_CurrentValidMoves.Any(pos => pos.ToPosition.x == newLocation.x && pos.ToPosition.y == newLocation.y))
                return true;

            
            return false;
        }

        private void ToggleTurn() {
            if (CurrentTurn == PieceType.Dwarves)
                CurrentTurn = PieceType.Trolls;
            else
                CurrentTurn = PieceType.Dwarves;

            MustChooseDwarveToKill = false;
            CurrentlySelected = null;
        }
    }
}
