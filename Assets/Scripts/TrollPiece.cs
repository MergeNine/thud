﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.Extensions;

namespace Assets.Scripts
{
    public class TrollPiece : IPiece
    {
        public PieceType Type { get; set; }

        public Vector3Int Position { get; set; }
        public bool IsAlive { get; set; }

        DebugTilemapInfo _DebugInfo;
        public TrollPiece(PieceType type, Vector3Int position, DebugTilemapInfo debugInfo = null)
        {
            Type = type;
            Position = position;
            IsAlive = true;
            _DebugInfo = debugInfo;
        }

        

        public IList<Move> GetValidMoves(PieceType[,] piecePositions, BoardTile[,] boardLayout)
        {
            List<Move> moves = GetNormalMoves(piecePositions, boardLayout).ToList();
            moves.AddRange(GetHurlMoves(piecePositions, boardLayout));
            return moves;
        }

        private IList<Move> GetNormalMoves(PieceType[,] piecePositions, BoardTile[,] boardLayout) {

            IList<Move> list = new List<Move>();

            for (int y = Position.y-1; y <= Position.y+1; y++)
            {
                for (int x = Position.x-1; x <= Position.x+1; x++)
                {
                    if (!PieceCalculations.IsTileOnBoard(x, y, boardLayout))
                        continue;

                    var boardPos = piecePositions[x, y];

                    if (boardPos == PieceType.None)
                        list.Add(new Move( MoveType.Normal, new Vector3Int(x, y, 0)));
                }
            }
            return list;
        }

        
        private IList<Move> GetHurlMoves(PieceType[,] piecePositions, BoardTile[,] boardLayout) {

            IList<Move> hurlMoves = new List<Move>();
            IList<Vector3Int> directions = PieceCalculations.GetPossibleDirections();

            foreach (var direction in directions) {
                int jumpAmount = PieceCalculations.GetNumberAdjacentPieces(boardLayout, piecePositions, Position, direction, PieceType.Trolls);
                var jumpDirection = direction.Scale(-1); //opposite from the direction we count trolls in
                var jumpPosition = Position + jumpDirection;

                if (jumpAmount == 0 || !PieceCalculations.IsTileOnBoard(jumpPosition.x, jumpPosition.y, boardLayout))
                    continue;

                //cannot jump over other trolls
                if (piecePositions[jumpPosition.x, jumpPosition.y] == PieceType.Trolls)
                    continue;

                for (int i = 2; i <= jumpAmount; i++) //must be at least 2 extra spaces to be a jump
                { 
                    jumpPosition = Position + jumpDirection.Scale(i);

                    if (_DebugInfo != null)
                        _DebugInfo.SetTile(jumpPosition.x, jumpPosition.y);

                    if (PieceCalculations.IsTileOnBoard(jumpPosition.x, jumpPosition.y, boardLayout) &&
                        PieceCalculations.IsAdjacentTo(jumpPosition, PieceType.Dwarves, boardLayout, piecePositions, _DebugInfo) &&
                        piecePositions[jumpPosition.x,jumpPosition.y] == PieceType.None)
                    {
                        hurlMoves.Add(new Move(MoveType.Hurl, jumpPosition));
                    }
                }
            }
            return hurlMoves;
        }

    }
}
