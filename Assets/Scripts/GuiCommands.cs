﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuiCommands : MonoBehaviour
{
    private GameRules _Game;
    
    GameObject _EndRoundButton;
    GameObject _ConfirmEndRoundPanel;
    GameObject _ContinueNextRoundButton;
    GameObject _ResetButton;
    public GameObject WelcomePanel;

    Text _HintTextBox;
    Text _GreenScoreBox;
    Text _RedScoreBox;
    Text _GreenTotalScoreBox;
    Text _RedTotalScoreBox;
    Color _Red = new Color(0.7924528f, 0.1918284f, 0);
    Color _Green = new Color(0, 0.6509434f, 0.3261408f);
    IList<GameResult> _CurrentScore;
    bool _ScoreboardUpdated;
    void Start()
    {
        GameObject boardObj = GameObject.Find("BoardGrid"); //TODO - find better way to include this dependency
        _Game = boardObj.GetComponentInChildren<GameRules>();
        
        _EndRoundButton = GameObject.Find("EndRoundButton");
        _ResetButton = GameObject.Find("ResetButton");
        _ConfirmEndRoundPanel = GameObject.Find("ConfirmEndRoundPanel");
        _ContinueNextRoundButton = GameObject.Find("ContinueToNextRoundButton");
        _ConfirmEndRoundPanel.SetActive(false);
        var textObj = GameObject.Find("HintTextBox");
        _HintTextBox = textObj.GetComponent<Text>();
        textObj = GameObject.Find("GreenScoreBox");
        _GreenScoreBox = textObj.GetComponent<Text>();
        textObj = GameObject.Find("RedScoreBox");
        _RedScoreBox = textObj.GetComponent<Text>();
        textObj = GameObject.Find("RedTotalScoreBox");
        _RedTotalScoreBox = textObj.GetComponent<Text>();
        textObj = GameObject.Find("GreenTotalScoreBox");
        _GreenTotalScoreBox = textObj.GetComponent<Text>();
        WelcomePanel.transform.SetAsFirstSibling();
        ClearScoreboard();
        //Hide buttons we don't need in the beginning
        
        _EndRoundButton.SetActive(false);
        _ScoreboardUpdated = false;
    }

    // Update is called once per frame
    void Update()
    {
        SetHintBoxColour();
        UpdateHintText();
        UpdateGameStatus();
    }


    private void UpdateGameStatus() {
        if(!_Game.IsGameEnded)
        {
            _ScoreboardUpdated = false;
            _EndRoundButton.SetActive(true);
        }
        
        

        if (_Game.IsGameEnded)
        {
            GameResult result = _Game.CalculateGameResult();
            _HintTextBox.text = result.WinningSide.ToString() + " Win!";
            
            if (!_ScoreboardUpdated)
            {
                _CurrentScore.Add(_Game.CalculateGameResult());
                UpdateScoreboard();
                _ScoreboardUpdated = true;
            }
            
        }
        if (_Game.IsGameEnded && !_Game.GameEndedManually)
        {
            _ContinueNextRoundButton.SetActive(true);
            _EndRoundButton.SetActive(false);
        }
        else
        {
            _ContinueNextRoundButton.SetActive(false);
            _EndRoundButton.SetActive(true);
        }
    }
    private void UpdateHintText()
    {

        if (_Game.CurrentTurn == PieceType.Dwarves)
        {
            _HintTextBox.text = "Dwarves to Move";
        }
        else if (_Game.CurrentTurn == PieceType.Trolls)
        {
            _HintTextBox.text = "Trolls to Move";

            if (_Game.MustChooseDwarveToKill)
                _HintTextBox.text += " - Select an adjacent Dwarve to kill";
        }
    }

    private void SetHintBoxColour() {
        if (_Game.CurrentTurn == PieceType.Dwarves)
        {
            if (_Game.AreDwarvesRed)
                _HintTextBox.color = _Red.linear;
            else
                _HintTextBox.color = _Green;
        }
        else if (_Game.CurrentTurn == PieceType.Trolls)
        {
            if (_Game.AreDwarvesRed)
                _HintTextBox.color = _Green;
            else
                _HintTextBox.color = _Red;
        }
    }

    public void UpdateScoreboard() {
        if (_CurrentScore == null)
            return;

        _GreenScoreBox.text = "";
        _RedScoreBox.text = "";
        int totalRed = 0;
        int totalGreen = 0;
        foreach (var result in _CurrentScore) {
            totalRed += result.RedScore.Score;
            totalGreen += result.GreenScore.Score;
            _RedScoreBox.text += result.RedScoreString;
            _GreenScoreBox.text += result.GreenScoreString;
        }
        _RedTotalScoreBox.text = "Red: " + totalRed;
        _GreenTotalScoreBox.text = "Green: " + totalGreen;
        _ScoreboardUpdated = true;
    }

    public void ClearScoreboard() {
        _CurrentScore = new List<GameResult>();
        _GreenScoreBox.text = "";
        _RedScoreBox.text = "";
        _ScoreboardUpdated = false;
    }
    /*public void ClickNextRound()
    {
        _Game.ResetBoard(!_Game.AreDwarvesRed);
        _ScoreboardUpdated = true;
        _ContinueButton.SetActive(false);
    }*/
    public void ClickResetGame() {
        _Game.ResetBoard(true);
        _ContinueNextRoundButton.SetActive(false);
        _CurrentScore = new List<GameResult>();
        ClearScoreboard();
        UpdateScoreboard();
        //_ContinueButton.SetActive(false);
        _ScoreboardUpdated = true;
    }

    

    public void ClickEndRound() {
        _ConfirmEndRoundPanel.SetActive(true);
        _EndRoundButton.SetActive(false);
        _ResetButton.SetActive(false);
        
    }



    public void ClickConfirmEndRound()
    {

        GameResult result = _Game.CalculateGameResult();
        _CurrentScore.Add(result);
        UpdateScoreboard();
        _Game.ResetBoard(!_Game.AreDwarvesRed);
        _ConfirmEndRoundPanel.SetActive(false);
        _EndRoundButton.SetActive(true);
        _ResetButton.SetActive(true);
    }
    public void ClickCancelEndRound()
    {
        _ConfirmEndRoundPanel.SetActive(false);
        _EndRoundButton.SetActive(true);
        _ResetButton.SetActive(true);
    }

    public void ClickContinueToNextRound()
    {
        UpdateScoreboard();
        _Game.ResetBoard(!_Game.AreDwarvesRed);
        _ConfirmEndRoundPanel.SetActive(false);
        _EndRoundButton.SetActive(true);
        _ResetButton.SetActive(true);
    }

    public void HideWelcomePanel() {
        WelcomePanel.SetActive(false);
    }

    public void OpenRulesURL() {
        Application.OpenURL("https://cdn.instructables.com/ORIG/FD5/9S6V/ID5S7XOD/FD59S6VID5S7XOD.pdf");
    }
}
