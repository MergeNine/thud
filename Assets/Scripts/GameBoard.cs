﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

/// <summary>
/// Add this script to a tilemap to have it generate tiles
/// </summary>
public class GameBoard : MonoBehaviour
{

    public TileBase BlackTile;

    public TileBase WhiteTile;

    public TileBase StoneTile;

    private Tilemap _TileMap;

    public BoardTile[,] _BoardLayout;

    [Tooltip("Number of tiles lengthwise or heightwise")]
    public int BoardWidth;

    // Start is called before the first frame update
    void Start()
    {
        if (BoardWidth % 2 == 0) //odd numbers only
            BoardWidth++;

        _BoardLayout = new BoardTile[BoardWidth, BoardWidth];
        _TileMap = GetComponent<Tilemap>(); //GetComponentInChildren<Tilemap>();
        _TileMap.ClearAllTiles();
        GenerateTiles();
        DrawTiles();
        
        /*SetupDwarveFastStartingPositions();
        SetupTrollFastStartingPositions();*/
        SetupDwarveStartingPositions();
        SetupTrollStartingPositions();
      
        
    }

    private void GenerateTiles()
    {

        int blanksForThisLine = (BoardWidth / 3);
        int increase = -1;
        BoardTileType tileToPlace = BoardTileType.NotAccessible;

        for (int y = 0; y < BoardWidth; y++)
        {
            for (int x = 0; x < BoardWidth; x++)
            {

                if ((x + y) % 2 == 1)
                    tileToPlace = BoardTileType.Black;
                else
                    tileToPlace = BoardTileType.White;

                if (x == BoardWidth / 2 && y == BoardWidth / 2)
                    tileToPlace = BoardTileType.ThudStone;

                if (x >= blanksForThisLine && x < BoardWidth - blanksForThisLine)
                {
                    _BoardLayout[x, y] = new BoardTile(tileToPlace);
                }
                else
                {
                    _BoardLayout[x, y] = new BoardTile(BoardTileType.NotAccessible);  
                }

            }
            if (blanksForThisLine < 0)
                blanksForThisLine = 0;

            if (blanksForThisLine == 0 && y >= 2 * BoardWidth / 3 - 1)
                increase = 1;

            blanksForThisLine = blanksForThisLine + increase;
        }

    }
    //For debugging
    private void SetupTrollFastStartingPositions()
    {
        int midPoint = BoardWidth / 2;
        _BoardLayout[midPoint +1, midPoint+1].IsStartingPosition = true;
        _BoardLayout[midPoint+1, midPoint+1].StartingPositionType = PieceType.Trolls;
    }
    private void SetupDwarveFastStartingPositions()
    {
        int midPoint = BoardWidth / 2;
        _BoardLayout[midPoint - 1, midPoint - 1].IsStartingPosition = true;
        _BoardLayout[midPoint - 1, midPoint - 1].StartingPositionType = PieceType.Dwarves;
        _BoardLayout[midPoint , midPoint - 1].IsStartingPosition = true;
        _BoardLayout[midPoint , midPoint - 1].StartingPositionType = PieceType.Dwarves;
    }
    //
    private void SetupTrollStartingPositions() {
        int midPoint = BoardWidth / 2;
        for (int y = midPoint-1; y <= midPoint + 1; y++)
        {
            for (int x = midPoint-1; x <= midPoint + 1; x++)
            {
                if (!(x == midPoint && y == midPoint))
                {
                    _BoardLayout[x, y].IsStartingPosition = true;
                    _BoardLayout[x, y].StartingPositionType = PieceType.Trolls;
                }
            }
        }
    }

    private void SetupDwarveStartingPositions()
    {
        int midPoint = BoardWidth / 2 ;

        for (int y = 0; y < BoardWidth; y++)
        {
            for (int x = 0; x < BoardWidth; x++)
            {
                var tileType = _BoardLayout[x, y].TileType;
                if ((tileType == BoardTileType.Black || tileType == BoardTileType.White) && x != midPoint && y != midPoint) {
                    if (IsNearEdgeX(x, y) || IsNearEdgeY(x, y))
                    {
                        _BoardLayout[x, y].IsStartingPosition = true;
                        _BoardLayout[x, y].StartingPositionType = PieceType.Dwarves;
                        
                    }
                }
            }
        }
    }

    private bool IsNearEdgeX(int x, int y) {
        return x == 0 || x == BoardWidth - 1 ||
                        (x > 0 && x < BoardWidth - 1 &&
                        (_BoardLayout[x - 1, y].TileType == BoardTileType.NotAccessible || _BoardLayout[x + 1, y].TileType == BoardTileType.NotAccessible));
    }

    private bool IsNearEdgeY(int x, int y)
    {
        return y == 0 || y == BoardWidth - 1 ||
                        (y > 0 && y < BoardWidth - 1 &&
                        (_BoardLayout[x, y - 1].TileType == BoardTileType.NotAccessible || _BoardLayout[x, y + 1].TileType == BoardTileType.NotAccessible));
    }

    
    public void DrawTiles()
    {
        for (int y = 0; y < BoardWidth; y++)
        {
            for (int x = 0; x < BoardWidth; x++)
            {
                if (_BoardLayout[x,y].TileType == BoardTileType.Black)
                    _TileMap.SetTile(new Vector3Int(x, y, 0), BlackTile);
                else if (_BoardLayout[x, y].TileType == BoardTileType.White)
                    _TileMap.SetTile(new Vector3Int(x, y, 0), WhiteTile);
                else if (_BoardLayout[x, y].TileType == BoardTileType.ThudStone)
                    _TileMap.SetTile(new Vector3Int(x, y, 0), StoneTile);
            }
        }
    }
    
    
    
    // Update is called once per frame
    void Update()
    {
        
    }
}
