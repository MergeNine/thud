﻿using Assets.Scripts.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class DwarvePiece : IPiece
    {
        public PieceType Type { get; set; }

        public Vector3Int Position { get; set; }
        public bool IsAlive { get; set; }

        public DwarvePiece(PieceType type, Vector3Int position)
        {
            Type = type;
            Position = position;
            IsAlive = true;
        }

       

        public IList<Move> GetValidMoves(PieceType[,] piecePositions, BoardTile[,] boardLayout)
        {
            List<Move> moves = GetNormalMoves(piecePositions, boardLayout).ToList();
            moves.AddRange( GetHurlMoves(piecePositions,boardLayout));
            return moves;
        }

        

        private IList<Move> GetNormalMoves(PieceType[,] piecePositions, BoardTile[,] boardLayout)
        {
            IList<Move> moves = new List<Move>();
            var directions = PieceCalculations.GetPossibleDirections();
            Vector3Int currentPosition;

            foreach (var dir in directions)
            {
                currentPosition = Position + dir;
                while (PieceCalculations.IsTileOnBoard(currentPosition.x, currentPosition.y, boardLayout) &&
                    piecePositions[currentPosition.x, currentPosition.y] == PieceType.None) {
                    moves.Add(new Move(MoveType.Normal, new Vector3Int(currentPosition.x, currentPosition.y, 0)));
                    currentPosition = currentPosition + dir;
                }
            }
            return moves;
        }


        private IList<Move> GetHurlMoves(PieceType[,] piecePositions, BoardTile[,] boardLayout)
        {

            IList<Move> hurlMoves = new List<Move>();
            IList<Vector3Int> directions = PieceCalculations.GetPossibleDirections();

            foreach (var direction in directions)
            {
                int jumpAmount = PieceCalculations.GetNumberAdjacentPieces(boardLayout, piecePositions, Position, direction, PieceType.Dwarves);
                var jumpDirection = direction.Scale(-1); //opposite from the direction we count dwarves in
                var jumpPosition = Position + jumpDirection;

                

                if (jumpAmount == 0 || !PieceCalculations.IsTileOnBoard(jumpPosition.x, jumpPosition.y, boardLayout))
                    continue;

                //cannot jump over other dwarves
                if (piecePositions[jumpPosition.x, jumpPosition.y] == PieceType.Dwarves)
                    continue;

                for (int i = 1; i <= jumpAmount; i++) //jump is how dwarves capture
                {
                    jumpPosition = Position + jumpDirection.Scale(i);
                    if (PieceCalculations.IsTileOnBoard(jumpPosition.x, jumpPosition.y, boardLayout) && 
                        piecePositions[jumpPosition.x, jumpPosition.y] == PieceType.Trolls)
                    {
                        hurlMoves.Add(new Move(MoveType.Hurl, jumpPosition));
                    }
                }
            }
            return hurlMoves;
        }
    }
}
