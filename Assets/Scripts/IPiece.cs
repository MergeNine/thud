﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public interface IPiece
    {
        PieceType Type { get; }

        Vector3Int Position { get; set; }

        IList<Move> GetValidMoves(PieceType[,] piecePositions, BoardTile[,] boardLayout);

        bool IsAlive { get; set; }

        
        
    }

    
}
