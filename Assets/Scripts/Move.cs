﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class Move
    {
        public Move(MoveType type, Vector3Int toPosition)
        {
            Type = type;
            ToPosition = toPosition;
        }

        public MoveType Type { get; set; }

        public UnityEngine.Vector3Int ToPosition { get; set; }
    }
}
