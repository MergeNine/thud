﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public enum PieceType
    {
        None = 0,
        Trolls,
        Dwarves
    }

    public enum BoardTileType {
        NotAccessible = 0,
        Black = 1,
        White = 2,
        ThudStone = 3 //Also not accessible
    }

    public enum MoveType {
        Normal,
        Hurl
    }
}
