﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts
{
    public class PieceFactory
    {
        public static IPiece GetPiece(PieceType type, Vector3Int position, DebugTilemapInfo debugInfo = null) {

            if (type == PieceType.Dwarves)
                return new DwarvePiece(type, position);
            else if (type == PieceType.Trolls)
                return new TrollPiece(type, position, debugInfo);
            
                return null;
        }
    }
}
