﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Assets.Scripts.Extensions;

namespace Assets.Scripts
{
    //calculations which could be used for any kind of piece
    public static class PieceCalculations
    {
        public static int GetNumberAdjacentPieces(BoardTile[,] boardLayout, PieceType[,] piecePositions, Vector3Int position, Vector3Int direction, PieceType piece)
        {
            //if we have trolls on either side of us for this direction, return 1
            var nextPosition = position; //the direction we're checking to see how many trolls there are
           
            int pieceCount = 0; 
            PieceType pieceAtPosition = piecePositions[nextPosition.x, nextPosition.y];
            while (IsTileOnBoard(nextPosition.x, nextPosition.y, boardLayout) && pieceAtPosition == piece)
            {
                pieceCount++;
                nextPosition = nextPosition + direction;
                if (!IsTileOnBoard(nextPosition.x, nextPosition.y, boardLayout))
                    break;

                pieceAtPosition = piecePositions[nextPosition.x, nextPosition.y];
            }
            return pieceCount; 
        }

        public static IList<Vector3Int> GetPossibleDirections() {
            IList<Vector3Int> directions = new List<Vector3Int>();
            directions.Add(new Vector3Int(1, 0, 0));
            directions.Add(new Vector3Int(0, 1, 0));
            directions.Add(new Vector3Int(0, -1, 0));
            directions.Add(new Vector3Int(-1, 0, 0));

            directions.Add(new Vector3Int(1, 1, 0));
            directions.Add(new Vector3Int(1, -1, 0));
            directions.Add(new Vector3Int(-1, 1, 0));
            directions.Add(new Vector3Int(-1, -1, 0));
            
            return directions;
        }
        public static bool IsTileOnBoard(int x, int y, BoardTile[,] boardLayout)
        {
            if (x < 0 || x >= boardLayout.GetLength(0) || y < 0 || y >= boardLayout.GetLength(1))
                return false;

            var tile = boardLayout[x, y];
            if (tile.TileType == BoardTileType.NotAccessible || tile.TileType == BoardTileType.ThudStone)
                return false;

            return true;
        }

        public static bool IsAdjacentTo(Vector3Int position, PieceType type, BoardTile[,] boardLayout, PieceType[,] piecePositions, DebugTilemapInfo debugInfo = null)
        {
            

            for (int y = position.y - 1; y <= position.y + 1; y++)
            {
                for (int x = position.x - 1; x <= position.x + 1; x++)
                {
                    /*if (debugInfo != null)
                        debugInfo.SetTile(x, y);*/

                    if (!IsTileOnBoard(x, y, boardLayout) || ( x == position.x && y == position.y))
                        continue;

                    if (piecePositions[x, y] == type)
                        return true;
                }
            }
            return false;
        }

        public static bool IsAdjacentTo(Vector3Int position, Vector3Int position2, PieceType type, BoardTile[,] boardLayout, PieceType[,] piecePositions, DebugTilemapInfo debugInfo = null)
        {


            for (int y = position.y - 1; y <= position.y + 1; y++)
            {
                for (int x = position.x - 1; x <= position.x + 1; x++)
                {
                    /*if (debugInfo != null)
                        debugInfo.SetTile(x, y);*/

                    if (!IsTileOnBoard(x, y, boardLayout) || (x == position.x && y == position.y))
                        continue;

                    if (piecePositions[x, y] == type && x == position2.x && y == position2.y)
                        return true;
                }
            }
            return false;
        }
    }
}
