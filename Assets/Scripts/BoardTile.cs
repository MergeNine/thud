﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public class BoardTile
    {
        public BoardTile(BoardTileType tileType, bool isStartingPosition, PieceType startingPositionType)
        {
            IsStartingPosition = isStartingPosition;
            StartingPositionType = startingPositionType;
            TileType = tileType;
        }

        public BoardTile(BoardTileType tileType) {
            TileType = tileType;
        }

        public bool IsStartingPosition { get; set; }

        public PieceType StartingPositionType { get; set; }

        public BoardTileType TileType { get; set; }
    }
}
