﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Assets.Scripts
{
    //used to see what we're trying to 
    public class DebugTilemapInfo
    {
        public TileBase DebugIconTile;

        public Tilemap DebugTilemap;

        public void SetTile(int x, int y) {
            if (DebugIconTile == null || DebugTilemap == null)
                return;

            DebugTilemap.SetTile(new Vector3Int(x, y, 0), DebugIconTile);
        }

        public void ClearTiles() {

            if (DebugTilemap == null)
                return;

            DebugTilemap.ClearAllTiles();
        }
        
    }
}
