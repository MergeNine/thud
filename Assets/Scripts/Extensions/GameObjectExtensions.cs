﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Extensions
{
    public static class GameObjectExtensions
    {

        public static IList<GameObject> FindChildrenWithTag(this GameObject parent, string childTag) {
          
            IList<GameObject> matches = new List<GameObject>();
            if (parent == null)
                return new List<GameObject>();

            var childCount = parent.transform.childCount;

            for (var i = 0; i < childCount; i++) {
                var child = parent.gameObject.transform.GetChild(i);
                var test = child.name;
                if (child.tag?.ToLower() == childTag?.ToLower())
                    matches.Add(child.gameObject);
            }
            return matches;
        }

        public static GameObject FindChild(this GameObject go, string name, bool recursiveSearch)
        {
            if (recursiveSearch)
            {
                var transform = go.transform;
                var childCount = transform.childCount;
                //Debug.LogError( go.name + " ChildCount: " + childCount);
                for (int i = 0; i < childCount; ++i)
                {
                    var child = transform.GetChild(i);
                    if (child.gameObject.name == name)
                        return child.gameObject;
                    GameObject result = child.gameObject.FindChild(name, recursiveSearch);
                    if (result != null) return result;
                }
                return null;
            }
            else
            {
                return GameObject.Find(name);
            }
        }

        public static IList<GameObject> FindChildrenWithTag(this GameObject go, string tag, bool recursiveSearch)
        {
            
            List<GameObject> foundItems = new List<GameObject>();
            if (recursiveSearch)
            {
                var transform = go.transform;
                var childCount = transform.childCount;

                
                for (int i = 0; i < childCount; i++)
                {
                    var child = transform.GetChild(i);
                    if (child.gameObject.tag.ToLower() == tag.ToLower())
                        foundItems.Add(child.gameObject);

                    var results = child.gameObject.FindChildrenWithTag(tag, recursiveSearch);
                    
                    if (results != null)
                    {
                        foundItems.AddRange(results);
                    }
                }
                
                return foundItems;
            }
            else
            {
                var transform = go.transform;
                var childCount = transform.childCount;

                //Debug.LogError( go.name + " ChildCount: " + childCount);
                for (int i = 0; i < childCount; ++i)
                {
                    var child = transform.GetChild(i);
                    if (child.gameObject.tag.ToLower() == tag.ToLower())
                        foundItems.Add(child.gameObject);
                }
                return foundItems;
            }
        }

        public static GameObject FindChild(this GameObject obj, string name)
        {
            Transform trans = obj.transform;
            Transform childTrans = trans.Find(name);
            if (childTrans != null)
            {
                return childTrans.gameObject;
            }
            else
            {
                return null;
            }
            
        }



        public static IList<GameObject> FindObjectWithComponent<T>() {
            var targets = UnityEngine.Object.FindObjectsOfType<GameObject>();
            return targets.Where(x => x.GetComponent<T>() != null).ToList();
        }
    }
}
