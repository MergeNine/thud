﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Extensions
{
    public static class Vector3IntExtensions
    {
        public static Vector3Int Scale(this Vector3Int original, int scalar)
        {
            return new Vector3Int(original.x * scalar, original.y * scalar, original.z * scalar);
        }
    }
}
