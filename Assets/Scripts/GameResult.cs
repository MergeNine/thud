﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public class GameResult
    {
        //public bool IsGameOver;
        public ScoreDetails RedScore;
        public ScoreDetails GreenScore;

        public PieceType WinningSide {
            get {
                if (RedScore.Score > GreenScore.Score)
                    return RedScore.Side;
                else if (RedScore.Score < GreenScore.Score)
                    return GreenScore.Side;
                else
                    return PieceType.None;
            }
        }

        


        public GameResult(ScoreDetails redScore, ScoreDetails greenScore) {
            RedScore = redScore;
            GreenScore = greenScore;
            //IsGameOver = true;
        }
        
        public string RedScoreString {
            get {
                return $"{RedScore.Side.ToString()} ({RedScore.Score})\n";
            }
        }
        public string GreenScoreString
        {
            get
            {
                return $"{GreenScore.Side.ToString()} ({GreenScore.Score})\n";
            }
        }
        

        public GameResult() {
            
        }
    }

    public class ScoreDetails {
        public ScoreDetails(PieceType side, int score)
        {
            Side = side;
            Score = score;
        }

        public ScoreDetails() {

        }

       public PieceType Side { get; set; }
        
       public int Score { get; set; }
    }
}
